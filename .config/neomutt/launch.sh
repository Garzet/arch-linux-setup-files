#!/bin/sh

# Try to unlock the keyring if it has not yet been unlocked.
if pass mail/migadu > /dev/null 2>&1
then
    # If the timer service is not active, start it to check for new mail
    # periodically.
    if ! systemctl is-active --user mbsync.timer > /dev/null 2>&1
    then
        systemctl start --user mbsync.timer
    fi

    # Start neomutt.
    neomutt

    # If no other neomutt processes are running.
    if ! pgrep -u $(id -ru) -x neomutt > /dev/null 2>&1
    then 
        # If the timer service is active, stop it.
        if systemctl is-active --user mbsync.timer > /dev/null 2>&1
        then
            systemctl stop --user mbsync.timer
        fi
    fi
else
    echo 'Failed to get access to the password store.'
    exit 1
fi
