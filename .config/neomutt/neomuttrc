# ------------------------------ GENERAL SETTINGS ------------------------------
# Do not pause when displaying a message. Makes neomutt more responsive.
set sleep_time = 0

# Do not break words in the middle when wrapping text.
set smart_wrap = yes

# Make newer messages appear on top of the mailbox screen.
set sort = "reverse-threads"

# Send notification when new mail is received.
set new_mail_command = "notify-send -a Neomutt 'New E-Mail' 'A new email message has been received.'"

# Time block if neomutt is in background (not focused).
set timeout = 60

# Period to check the folders for new mail when neomutt is focused.
set mail_check = 5

# Periodically calculate message statistics (for example, unread message count).
set mail_check_stats = yes

# Do not add a plus sign to URLs that wrap.
unset markers

# Root folder where mail for all accounts is located.
set folder = "$HOME/.local/share/mail"

# Define character set of the terminal.
set charset = "utf-8"

# Index format string. Message number, flags, date, sender/receiver and subject.
set index_format = "%4C %Z %{%b %d} %-15.15L %s"

# Do not ask every time when appending messages to some mailbox.
set noconfirmappend

# Do not ask every time when deleting messages. 
set delete = yes

# Signature file
set signature = ""
# ------------------------------------------------------------------------------


# ---------------------------------- SIDEBAR -----------------------------------
# Sidebar options.
set sidebar_visible
set sidebar_format = "%D%* %?N?%N/?%S"
set sidebar_width = 15
# ------------------------------------------------------------------------------


# ------------------- INCLUSION OF OTHER CONFIGURATION FILES -------------------
# Colorscheme.
source colors.neomuttrc

# Keybindings.
source keys.neomuttrc

# User specific settings (account credentials and settings for each account).
source $XDG_CONFIG_HOME/pc-specifics/mail/accounts/accounts.neomuttrc
# ------------------------------------------------------------------------------
