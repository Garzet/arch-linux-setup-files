function n --wraps nnn --description 'support nnn quit and change directory'
    set NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"

    command nnn -T v $argv

    if test -e $NNN_TMPFILE
        source $NNN_TMPFILE
        rm $NNN_TMPFILE
    end
end
