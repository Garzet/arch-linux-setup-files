if status is-interactive
    function fish_mode_prompt
    end

    function fish_prompt
        printf '\n%s┏━[%s%s%s@%s%s%s]━[%s%s%s]\n┗━━>%s '      \
                (set_color brblack)                           \
                (set_color blue)     $USER                    \
                (set_color brblack)                           \
                (set_color bryellow) $hostname                \
                (set_color brblack)                           \
                (set_color brblue)   $(prompt_pwd -d 3 -D 15) \
                (set_color brblack)                           \
                (set_color normal)
    end
    
    function fish_user_key_bindings
        fish_default_key_bindings -M insert
        fish_vi_key_bindings --no-erase insert
        bind -M normal -M insert ctrl-space accept-autosuggestion 
    end

    set fish_cursor_default     block
    set fish_cursor_insert      line
    set fish_cursor_replace_one underscore
    set fish_cursor_visual      block
    set -U fish_greeting
end
