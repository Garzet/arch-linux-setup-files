#!/bin/sh

# File where output of a synchronization command is stored.
NEXCLOUD_LOG_FILE='/tmp/nextcloud.log'

# Print a message for beginning of synchronization.
echo 'Nextcloud synchronization started...'

# Get the nextcloud password from the password store.
if NEXTCLOUD_PASSWORD=$(pass $NEXTCLOUD_PASSWORD_STORE)
then 
    if nextcloudcmd --user "$NEXTCLOUD_USERNAME"   \
                    --password $NEXTCLOUD_PASSWORD \
                    "$XDG_CLOUD_HOME"              \
                    "$NEXTCLOUD_SERVER_ADDRESS" > "$NEXCLOUD_LOG_FILE" 2>&1
    then # Synchronization successful.
        echo 'Done!'
    else # Synchronization failed, log an error message.
        echo 'Nextcloud synchronization failed.'
        echo "Log written to '$NEXCLOUD_LOG_FILE'."
        exit 1
    fi
else # Failed to get the password from the password store.
    echo 'Failed to get the Nextcloud password.'
    exit 1
fi
