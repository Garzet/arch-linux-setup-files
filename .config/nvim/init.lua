----------------------------------- OPTIONS ------------------------------------
-- Set the encoding.
vim.opt.encoding       = 'utf-8'
vim.opt.fileencoding   = 'utf-8'

-- Tab size and mandate using spaces instead of tabs.
vim.opt.expandtab      = true
vim.opt.tabstop        = 4
vim.opt.shiftwidth     = 4

-- Break lines at word boundary if line is longer than text width.
vim.opt.textwidth      = 80

-- Line numbering style.
vim.opt.number         = true
vim.opt.relativenumber = true

-- Use case insensitive search, except when capital letters are involved.
vim.opt.ignorecase     = true
vim.opt.smartcase      = true

-- Display cursor position on the last line of the screen or in the status line
-- of a window.
vim.opt.ruler          = true

-- Enable mouse for all modes.
vim.opt.mouse          = 'a'

--  Splits open at the bottom and right.
vim.opt.splitbelow     = true
vim.opt.splitright     = true

-- Set the height of the autocomplete menu.
vim.opt.pumheight      = 10

-- When exiting a file with non-saved changes, show a save dialog.
vim.opt.confirm        = true

-- Hilight line number differently on the cursor line.
vim.opt.cursorline     = true
vim.opt.cursorlineopt  = 'number'

-- Time to wait for a mapped sequence to complete (in millisenconds).
vim.opt.timeoutlen     = 1000

-- Do not wrap long lines.
vim.opt.wrap           = false

-- Numer of lines/characters to keep visible around the cursor.
vim.opt.scrolloff      = 8
vim.opt.sidescrolloff  = 1

vim.opt.completeopt    = 'menu,menuone,noselect'
--------------------------------------------------------------------------------

----------------------------------- KEYMAP -------------------------------------
-- Make space the leader key.
vim.keymap.set("n", "<SPACE>", "<Nop>", { silent = true, remap = false })
vim.g.mapleader = " "

-- Remap Croatian characters to standard US keyboard characters. This does not
-- remap for insert mode.
vim.keymap.set('', 'ž', '\\', { remap = true })
vim.keymap.set('', 'č', ';', { remap = true })
vim.keymap.set('', 'ć', "'", { remap = true })
vim.keymap.set('', 'š', '[', { remap = true })
vim.keymap.set('', 'đ', ']', { remap = true })
vim.keymap.set('', 'Č', ':', { remap = true })
vim.keymap.set('', 'Ć', '"', { remap = true })
vim.keymap.set('', 'Š', '{', { remap = true })
vim.keymap.set('', 'Đ', '}', { remap = true })
vim.keymap.set('', 'Ž', '|', { remap = true })

-- Shorter split window navigation key binds.
vim.keymap.set('n', '<C-h>', '<C-w>h')
vim.keymap.set('n', '<C-j>', '<C-w>j')
vim.keymap.set('n', '<C-k>', '<C-w>k')
vim.keymap.set('n', '<C-l>', '<C-w>l')

-- Resize with arrows
vim.keymap.set('n', '<C-Up>', ':resize +2<CR>', { silent = true })
vim.keymap.set('n', '<C-Down>', ':resize -2<CR>', { silent = true })
vim.keymap.set('n', '<C-Left>', ':vertical resize -2<CR>', { silent = true })
vim.keymap.set('n', '<C-Right>', ':vertical resize +2<CR>', { silent = true })

-- Redo remap.
vim.keymap.set('n', 'U', ':redo<CR>', { silent = true })

-- Toggle line wrap.
vim.keymap.set('n', '<F4>', ':set wrap!<CR>', { silent = true })

-- Toggle spell checker.
vim.keymap.set('', '<F7>', ':setlocal spell! spelllang=en_us<CR>')
vim.keymap.set('', '<F8>', ':setlocal spell! spelllang=hr<CR>')

-- Find line longer than 80 characters.
vim.keymap.set('n', '<F11>', [[/\%>80v.\+<CR>]])

-- Find line with whitespace at the end.
vim.keymap.set('n', '<F12>', [[/\s\+$<CR>]])

-- Open a file.
vim.keymap.set('n', '<C-o>', ':edit<SPACE>')

-- Open a file in a new tab.
vim.keymap.set('n', '<C-t>', ':tabedit<SPACE>')

-- Split horizontally and vertically.
vim.keymap.set('n', '<C-r>', ':vsplit<SPACE>')
vim.keymap.set('n', '<C-e>', ':split<SPACE>')

-- Go to next and previous tab.
vim.keymap.set('n', 'gh', ':tabnext<CR>', { silent = true })
vim.keymap.set('n', 'gf', ':tabprevious<CR>', { silent = true })

-- Move current tab to the right and to the left.
vim.keymap.set('n', 'gH', ':tabm +1<CR>', { silent = true })
vim.keymap.set('n', 'gF', ':tabm -1<CR>', { silent = true })

-- Rebind scrolling by one line.
vim.keymap.set('n', '<C-x>', '<C-y>')
vim.keymap.set('n', '<C-y>', '<C-e>')

-- Go to the previously open buffer.
vim.keymap.set('n', '<SPACE>n', ':b#<CR>', { silent = true })

-- Do not lose copied text when pasting over something in visual mode.
vim.keymap.set('v', 'p', '"_dP')

-- Exit terminal mode with escape.
vim.keymap.set('t', '<ESC>', '<C-\\><C-n>')
--------------------------------------------------------------------------------


----------------------------------- PLUGINS ------------------------------------
--------- PLUGIN MANAGER (PAQ) ------------
require('paq')({
    'savq/paq-nvim',
    'neovim/nvim-lspconfig',
    'nvim-treesitter/nvim-treesitter',
    'beyondmarc/glsl.vim',
    'aklt/plantuml-syntax',

    -- Commenting.
    'terrortylor/nvim-comment',

    -- Autocompletion and its dependencies.
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/nvim-cmp',

    -- Debugging.
    'nvim-neotest/nvim-nio',
    'mfussenegger/nvim-dap',
    'rcarriga/nvim-dap-ui',

    -- Snippets and its integration into autocompletion.
    'L3MON4D3/LuaSnip',
    'saadparwaiz1/cmp_luasnip',

    -- Telescope and its dependencies.
    'nvim-lua/popup.nvim',
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope.nvim',

    -- Markdown viewer.
    { "toppair/peek.nvim", { build = 'deno task --quiet build:fast' } },

    -- REPL integration.
    'Vigemus/iron.nvim',

    -- Colorschemes.
    'Mofiqul/dracula.nvim',
    'navarasu/onedark.nvim'

    -- Lilypond suite and its dependencies.
    --'martineausimon/nvim-lilypond-suite',
    --'MunifTanjim/nui.nvim'
})
-------------------------------------------

--------------- TREESITTER ----------------
require('nvim-treesitter.configs').setup({
    ensure_installed = {
        'c', 'cpp', 'cmake', 'meson', 'make', 'ninja', 'doxygen',
        'rust',
        'python', 'lua', 'java', 'c_sharp',
        'go', 'gomod', 'gosum',
        'html', 'htmldjango', 'css', 'typescript', 'sql', 'javascript',
        'bash', 'fish', 'awk',
        'ini', 'json', 'toml', 'yaml', 'xml',
        'markdown',
        'vim', 'vimdoc',
        'latex', 'bibtex',
        'comment', 'csv', 'diff',
        'dockerfile',
        'git_config', 'git_rebase', 'gitignore', 'gitattributes',
        'glsl',
        'ledger',
        'gpg',
        'objdump', 'proto'
    },
    sync_install = false,
    auto_install = true,
    ignore_install = {},
    highlight = { enable = true },
    modules = {}
})
-------------------------------------------

--------------- DEBUGGING -----------------
vim.keymap.set('n', '<leader>dc', ':lua require("dap").continue()<CR>');
vim.keymap.set('n', '<leader>dr', ':lua require("dap").restart()<CR>');
vim.keymap.set('n', '<leader>dt', ':lua require("dap").terminate()<CR>');
vim.keymap.set('n', '<leader>j', ':lua require("dap").step_over()<CR>');
vim.keymap.set('n', '<leader>l', ':lua require("dap").step_into()<CR>');
vim.keymap.set('n', '<leader>k', ':lua require("dap").step_out()<CR>');
vim.keymap.set('n', '<leader>db', ':lua require("dap").toggle_breakpoint()<CR>');
vim.keymap.set('n', '<leader>dB', ':lua require("dap").set_breakpoint(' ..
    'vim.fn.input("Condition: "))<CR>');
vim.keymap.set('n', '<leader>dl', ':lua require("dap").set_breakpoint(' ..
    'nil, nil, vim.fn.input("Log point message: "))<CR>');

local dap = require('dap')
dap.adapters.cpp = {
    type = 'executable',
    command = '/usr/bin/lldb-vscode',
    name = 'cpp'
}

-- Open/close DAP UI automatically when starting/stopping debugging.
local dapui = require("dapui")
dapui.setup()
dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
end
-------------------------------------------

-------------- NVIM-COMMENT ---------------
require('nvim_comment').setup({
    -- Do not comment empty lines.
    comment_empty = false,
    -- Do not create default mappings.
    create_mappings = false
})

vim.keymap.set({ 'n', 'v' }, '<LEADER>c', ':CommentToggle<CR>',
    { silent = true })
-------------------------------------------

-------------- AUTOCOMPLETION -------------
local cmp = require('cmp')

local luasnip = require('luasnip')

cmp.setup({
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    mapping = {
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ['<Tab>'] = cmp.mapping(
            function(fallback)
                if cmp.visible() then
                    cmp.select_next_item()
                elseif luasnip.expandable() then
                    luasnip.expand()
                elseif luasnip.expand_or_jumpable() then
                    luasnip.expand_or_jump()
                else
                    fallback() -- Sends the already mapped key (likely <TAB>).
                end
            end,
            { 'i', 's' }
        ),
        ['<S-Tab>'] = cmp.mapping(
            function(fallback)
                if cmp.visible() then
                    cmp.select_prev_item()
                elseif luasnip.jumpable(-1) then
                    luasnip.jump(-1)
                else
                    fallback() -- Sends the already mapped key (likely <TAB>).
                end
            end,
            { 'i', 's' }
        ),
        ['<C-e>'] = cmp.mapping {
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        },
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ['<C-k>'] = cmp.mapping.select_prev_item(),
        ['<C-j>'] = cmp.mapping.select_next_item(),
        ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-1), { 'i', 'c' }),
        ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(1), { 'i', 'c' })
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
    }, {
        { name = 'luasnip' },
    }, {
        { name = 'buffer' },
    }),
    PreselectMode = cmp.PreselectMode.None
})

-- Setup lspconfig.
local capabilities = require('cmp_nvim_lsp').default_capabilities(
    vim.lsp.protocol.make_client_capabilities()
)
-------------------------------------------

------------------- LSP -------------------
local lspconfig = require('lspconfig')

-- Executes after the language server attaches to the buffer.
local on_attach = function(client, bufnr)
    local opts = { silent = true, buffer = bufnr }
    vim.keymap.set('n', '<SPACE>h', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', '<SPACE>f', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', '<F2>', vim.lsp.buf.rename, opts)
    vim.keymap.set('n', '<F3>', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<F9>', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', '<F10>', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'FF', vim.lsp.buf.format, opts)

    vim.keymap.set('n', '<SPACE>e', vim.diagnostic.open_float, opts)

    -- Telescope LSP mappings.
    local t = require('telescope.builtin')
    vim.keymap.set('n', '<SPACE>p', t.lsp_dynamic_workspace_symbols, opts)

    -- Disable language server semantic highlighting.
    client.server_capabilities.semanticTokensProvider = nil
end

-- Executes after clangd language server attaches to the buffer.
local clangd_on_attach = function(_, bufnr)
    on_attach(_, bufnr)

    -- Set comment style for C/C++.
    vim.api.nvim_set_option_value('commentstring', '// %s', {buf = bufnr})

    -- Function for source-header switch but with the required split command.
    local switch_source_header_splitcmd = function(splitcmd)
        local params = { uri = vim.uri_from_bufnr(bufnr) }
        vim.lsp.buf_request(bufnr, 'textDocument/switchSourceHeader', params,
            function(err, result)
                if err then error(tostring(err)) end
                if not result then
                    print('Corresponding file could not be determined.')
                    return
                end
                vim.api.nvim_command(splitcmd .. ' ' .. vim.uri_to_fname(result))
            end
        )
    end

    local opts = { silent = true, buffer = bufnr }
    vim.keymap.set('n', '<LEADER>v',
        function() switch_source_header_splitcmd('vsplit') end, opts)
    vim.keymap.set('n', '<LEADER>x',
        function() switch_source_header_splitcmd('split') end, opts)
    vim.keymap.set('n', '<LEADER>o',
        function() switch_source_header_splitcmd('edit') end, opts)
end

local get_clangd_cmd = function()
    if vim.fn.getcwd():find('^/home/rudolf/dev/esp/') ~= nil then
        return '/home/rudolf/dev/esp/esp-clang/bin/clangd'
    end
    return 'clangd'
end

-- Setup clangd for C/C++ development.
lspconfig.clangd.setup({
    capabilities = capabilities,
    cmd          = { get_clangd_cmd(),
                     '--header-insertion=never',
                     '--query-driver=**' }, -- Whitelist any compiler (e.g. for ESP-IDF toolchain).
    on_attach    = clangd_on_attach,
    flags        = { debounce_text_changes = 150 },
})

-- Setup rls for rust development.
lspconfig.rust_analyzer.setup({
    capabilities = capabilities,
    on_attach    = on_attach,
})

-- Setup lua for Lua development.
local lua_ls_root_path = '/usr/lib/lua-language-server'
local lua_ls_binary    = '/usr/bin/lua-language-server'

local runtime_path     = vim.split(package.path, ';')
table.insert(runtime_path, 'lua/?.lua')
table.insert(runtime_path, 'lua/?/init.lua')

lspconfig.lua_ls.setup({
    capabilities = capabilities,
    cmd          = { lua_ls_binary, '-E', lua_ls_root_path .. '/main.lua' },
    on_attach    = on_attach,
    settings     = {
        Lua = {
            runtime = { version = 'lua', path = runtime_path },
            diagnostics = { globals = { 'vim' } },
            -- Make the server aware of Neovim runtime files.
            workspace = { library = vim.api.nvim_get_runtime_file('', true) },
            -- Do not send telemetry data.
            telemetry = { enable = false }
        }
    }
})

-- Setup pyright for python development.
lspconfig.pyright.setup({
    on_attach = on_attach,
    settings = {
        pyright = { autoImportCompletion = true, },
        python = {
            analysis = {
                autoSearchPaths = true,
                diagnosticMode = 'openFilesOnly',
                useLibraryCodeForTypes = true,
                typeCheckingMode = 'off'
            }
        }
    }
})

-- Setup gopls for go development.
lspconfig.gopls.setup({
    capabilities = capabilities,
    on_attach    = on_attach
})

-- Setup tsserver for javascript and typescript development.
lspconfig.ts_ls.setup({
    capabilities = capabilities,
    on_attach    = on_attach
})

-- Setup qmlls for Qt Quick development.
lspconfig.qmlls.setup({
    cmd          = {'qmlls6'},
    capabilities = capabilities,
    on_attach    = on_attach
})

-- Setup dartls for Dart development.
lspconfig.dartls.setup({
    cmd          = {'/usr/bin/dart', 'language-server', '--protocol=lsp'},
    capabilities = capabilities,
    on_attach    = on_attach
})

-- Global diagnostics configuration.
vim.diagnostic.config({
    virtual_text = false, -- Disable inline diagnostics.
    underline    = true   -- Underline errors and warnings.
})

vim.fn.sign_define('DiagnosticSignError', { texthl = 'DiagnosticError', text = '>', })
vim.fn.sign_define('DiagnosticSignWarn', { texthl = 'DiagnosticWarn', text = '>', })
vim.fn.sign_define('DiagnosticSignHint', { texthl = 'DiagnosticWarn', text = '>', })
vim.fn.sign_define('DiagnosticSignInfo', { texthl = 'DiagnosticWarn', text = '>', })
-------------------------------------------

--------------- TELESCOPE -----------------
local actions = require('telescope.actions')
require('telescope').setup({
    defaults = {
        file_ignore_patterns = { 'build/', '.cache', 'target/' },
        mappings             = {
            i = {
                -- Disable old mappings.
                ['<C-n>'] = false,
                ['<C-p>'] = false,

                -- Add new mappings.
                ['<C-j>'] = actions.move_selection_next,
                ['<C-k>'] = actions.move_selection_previous,
                ['<ESC>'] = actions.close,
            }
        }
    }
})

vim.keymap.set('n', '<C-p>', ':Telescope find_files<CR>')
vim.keymap.set('n', '<SPACE>g', ':Telescope live_grep<CR>')
vim.keymap.set('n', '<SPACE>b', ':Telescope buffers<CR>')
-------------------------------------------

------------------ IRON -------------------
require("iron.core").setup({
    config = {
        repl_definition = {
            python = {
                command = { "ipython", "--no-autoindent" }
            }
        },
        repl_open_cmd = require('iron.view').split.vertical.botright("50%"),
    },
    keymaps = {
        send_motion       = "<SPACE>sm",
        visual_send       = "<SPACE>sv",
        send_file         = "<SPACE>sf",
        send_until_cursor = "<SPACE>sc",
        send_line         = "<SPACE>sl",
        cr                = "<SPACE>s<cr>",
        exit              = "<SPACE>sq",
        clear             = "<SPACE>cl",
    },
    highlight = {
        italic = true
    },
    ignore_blank_lines = true,
})

vim.keymap.set('n', '<SPACE>rs', ':IronRepl<cr>')
vim.keymap.set('n', '<SPACE>rr', ':IronRestart<cr>')
-------------------------------------------

------------ MARKDOWN PREVIEW -------------
-- default config:
require('peek').setup({
    auto_load = true,
    close_on_bdelete = true,
    theme = 'dark',
    update_on_change = true,
    app = { 'firefox', '--new-window' },
    filetype = { 'markdown' },
    throttle_at = 200000,
    throttle_time = 'auto',
})
vim.api.nvim_create_user_command('PeekOpen', require('peek').open, {})
vim.api.nvim_create_user_command('PeekClose', require('peek').close, {})
-------------------------------------------
--------------------------------------------------------------------------------


-------------------------------- COLORS ----------------------------------------
-- Set the color scheme.
require('onedark').setup({
    style = 'deep',
    transparent = true,
})
require('onedark').load()
--------------------------------------------------------------------------------


------------------------------- FILETYPES --------------------------------------
vim.filetype.add({ extension = { typ = 'typst' } })
--------------------------------------------------------------------------------
