" Detect Lilypond files.
au! BufNewFile,BufRead *.ly,*.ily set ft=lilypond
