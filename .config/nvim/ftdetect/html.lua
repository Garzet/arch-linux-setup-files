-- Set HTML syntax to Django variant so that Jinja and Django variants are
-- highlighted nicely.
vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
    pattern = { "*.html" },
    command = "set syntax=htmldjango"
})
