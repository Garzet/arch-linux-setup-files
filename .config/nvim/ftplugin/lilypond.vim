" Only do this when not done yet for this buffer.
if exists("b:did_ftplugin")
    finish
endif
let b:did_ftplugin = 1 " Mark that plugin has been loaded.

" Save current file and create PDF output.
map <buffer> <F5> :w<CR>
                \ :!lilypond --pdf "%"<CR>

" Save current file, create PDF output and play MIDI file using Timidity. 
map <buffer> <F6> :w<CR>
                \ :!lilypond --pdf %<CR>
                \ :!timidity "%<.midi"<CR>
