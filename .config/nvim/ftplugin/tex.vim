" Only do this when not done yet for this buffer.
if exists("b:did_ftplugin")
    finish
endif
let b:did_ftplugin = 1 " Mark that plugin has been loaded.

set commentstring=%%s
    
" Save all files and make the LaTeX document.
noremap <buffer> <F5> :wall<CR>
                    \ :!latexmk<CR>

" Open a matching source or header file in a vertical split.
noremap <buffer> <Leader>v :call tex#OpenMatchingClassOrTex('vsplit')<CR>

" Open a matching source or header file in a horizontal split.
noremap <buffer> <Leader>x :call tex#OpenMatchingClassOrTex('split')<CR>
