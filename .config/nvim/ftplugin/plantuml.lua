-- Save all files and create the UML as PDF document.
vim.api.nvim_buf_set_keymap(0,
    'n', '<F5>', ':w<CR> :call plantuml#MakeUmlAsPdf()<CR>',
    {noremap = true, silent = true})
