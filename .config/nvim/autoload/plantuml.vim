" Make UML as PDF. This first calls PlantUML to create SVG file and then uses
" inkscape to convert the SVG file to PDF. The SVG file is removed afterwards.
function! plantuml#MakeUmlAsPdf()
   let l:file     = "'" . @% . "'" " Filename with extension.
   let l:filename = expand('%:r')  " Filename without extension.
   let l:svgfile  = "'" . l:filename . ".svg'"
   let l:pdffile  = "'" . l:filename . ".pdf'"
   :execute "!plantuml -tsvg " . l:file . 
       \ " && inkscape --export-filename=". l:pdffile . " " . l:svgfile 
       \ " && rm " . l:svgfile
endfun
