#!/bin/sh
# Find all image files in the directory and open them in the image viewer.

if [ $# -eq 0 ]; then
    echo "Usage: ${0##*/} PICTURES"
    exit
fi

[ "$1" = '--' ] && shift

# Extracts the absolute path of the file.
abspath () {
    case "$1" in
        /*) printf "%s\n" "$1";;
        *)  printf "%s\n" "$PWD/$1";;
    esac
}


# Use version sort to sort the files.
listfiles () {
    find -L "$(dirname "$1")" -maxdepth 1 -type f -iregex \
      '.*\(jpe?g\|bmp\|png\|gif\|ppm\|pgm\|pbm\)$' -print0 | sort -Vz
}

# Absolute path of the selected file.
TARGET="$(abspath "$1")"

# Offset of selected file from the begining of all image files.
OFFSET="$(listfiles "$TARGET" | grep -m 1 -ZznF "$TARGET" | cut -d ':' -f 1)"

if [ -n "$OFFSET" ]; then # Check if offset has been properly calculated.
    # Open image at the given offset from the begining of all image files.
    listfiles "$TARGET" | xargs -0 imv -n "$OFFSET" --
else
    imv -- "$@" # Fallback option - just open the selected file.
fi
