# Remember commands between sessions.
set history save
set history filename ~/.gdb_history

# Do not print addresses when displaying their contents.
set print address off

# Print structures in an indented format with one member per line
set print pretty on

# Disable confirmation for commands.
set confirm off

# Disable auto-load protection.
set auto-load safe-path /
